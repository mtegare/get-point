const fetch = require('node-fetch');
const cheerio = require('cheerio');
const fs = require('async-file');
const delay = require('delay');
const readlineSync = require('readline-sync');

function getString(start, end, all) {
	const regex = new RegExp(`${start}(.*?)${end}`);
	const str = all
	const result = regex.exec(str);
	return result;
}

const getCookie = () => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/auth/login', {
        method: 'GET',
    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
            csrf: $('input[name=decide_csrf]').attr('value')
        }

        resolve(result)
    })
    .catch(err => reject(err))
});

const login = (deviceId, deviceIds, session, csrf, email, pass) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/auth/login', {
        method: 'POST',
        headers: {
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            cookie: `scs=1; ${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; scs=1; ins-gaSSId=19abc7ed-2a04-6af2-78e8-5ac10ba384f1_1569984106; _p1K4r_=true; pikar_redirect=true; token=QzjCgYHq4HaIEetYjL6WlJpYIHutelgx; refresh_token=uhEppIhB0oBVXCmh3NpDEAHtdYZyWYKZ; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; _gat_UA-102334128-3=1; insdrSV=14; ${session}`,
            host: 'www.marlboro.id',
            origin: 'https://www.marlboro.id',
            referer: 'https://www.marlboro.id/auth/login',
            'sec-fetch-mode': 'cors',
            'x-requested-with': 'XMLHttpRequest',
            'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'
        },
        body: `email=${email}&password=${pass}&remember_me=remember_me&ref_uri=/&decide_csrf=${csrf}&param=&exception_redirect=false`

    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
        }

        resolve(result)
    })
    .catch(err => reject(err))
});

const getToken = (realCok) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/', {
        method: 'GET',
        headers: {
        	Host: 'www.marlboro.id',
        	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
        	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        	'Accept-Language': 'en-US,en;q=0.5',
			'Accept': 'application/json',
			'Accept-Encoding': 'gzip, deflate, br',
			'DNT': 1,    
			'Connection': 'keep-alive',
			'Upgrade-Insecure-Requests': 1,
			cookie: realCok
        }
    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
        }
        resolve(result)
    })
    .catch(err => reject(err))
});

const getCookieKuis = (realCokk) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/discovered/passion-quiz/question-3', {
        method: 'GET',
        headers: {
        	Host: 'www.marlboro.id',
        	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
        	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        	'Accept-Language': 'en-US,en;q=0.5',
			'Accept': 'application/json',
			'Accept-Encoding': 'gzip, deflate, br',
			'DNT': 1,    
			'Connection': 'keep-alive',
			'Upgrade-Insecure-Requests': 1,
			Cookie: realCokk
        }
    }).then(async res => {
        const $ = cheerio.load(await res.text());
        const result = {
            cookie: res.headers.raw()['set-cookie'],
            csrf: $('input[name=decide_csrf]').attr('value')
        }
        resolve(result)
    })
    .catch(err => reject(err))
});

const kuis = (realCokkk, csrf2) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/discovered/passion-quiz-insert', {
        method: 'POST',
        headers: {
            Host: 'www.marlboro.id',
            Origin: 'https://www.marlboro.id',
            Referer: 'https://www.marlboro.id/discovered/passion-quiz/question-3',
        	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
        	'Accept': '*/*',
        	'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        	'Accept-Language': 'id-ID,id;q=0.9,en-US;q=0.8,en;q=0.7',
			'Accept-Encoding': 'gzip, deflate, br',
			'Connection': 'keep-alive',
			Cookie: realCokkk
        },
        body: `answer=3&decide_csrf=${csrf2}`

    })
    .then(res => res.json())
    .then(result => resolve(result))
    .catch(err => reject(err))
});

const getPoint = (realCokkkk) => new Promise((resolve, reject) => {
    fetch('https://www.marlboro.id/profile', {
        method: 'GET',
        headers: {
        	Host: 'www.marlboro.id',
        	'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
        	'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        	'Accept-Language': 'en-US,en;q=0.5',
        	'referer': 'https://www.marlboro.id/',
			'Accept': 'application/json',
			'Accept-Encoding': 'gzip, deflate, br',
			'Connection': 'keep-alive',
			'Upgrade-Insecure-Requests': 1,
			cookie: realCokkkk
        }
    })
    .then(res => res.text())
    .then(result => {
    	const $ = cheerio.load(result);
    	const get = $('div.point-container-wrapper div.point').text();
    	resolve(get)
    })
    .catch(err => reject(err))
});

(async () => {
	console.log('Bot Claim 10k Point');
	const file = await readlineSync.question('Input FIle Akun Marlboro (Email|Pass): ');
    const akun = await fs.readFile(file, 'utf8');
    let akunList = [];
	await akunList.push(akun.toString().replace(/\r\n|\r|\n/g, " ").split(" "));
    if(akunList[0].length > 0) {
	    console.log('Total Ada: '+akunList[0].length+' Akun');
	    for (var i = 0; i < akunList[0].length; i++) {
	    const akunn = akunList[0][i];
	    const pecah = akunn.split('|');
	    const email = pecah[0];
	    const pass = pecah[1];
	    const cookie = await getCookie();
		const cok = cookie.cookie.join().split(',');
		const deviceId = cok[0].split(';')[0];
		const deviceIds = cok[2].split(';')[0];
		const session = cok[4].split(';')[0];
		const csrf = cookie.csrf;
		const log = await login(deviceId, deviceIds, session, csrf, email, pass);
		const realCok = `${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; scs=1; ins-gaSSId=19abc7ed-2a04-6af2-78e8-5ac10ba384f1_1569984106; _p1K4r_=true; pikar_redirect=true; ${log.cookie.join().split(',')[4].split(';')[0]}; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; insdrSV=16; token=gWaIXv7WEdx3uugu2e7ReJqxDuzptUlX; refresh_token=Ubia8eQnQawAe92hcIAjH4IxFsHUcMh7; ${log.cookie.join().split(',')[6].split(';')[0]}`;
		const token = await getToken(realCok);
		const tokenn = token.cookie.join().split(',');
		const realCokk = `scs=1; scs=1; ${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; scs=1; _p1K4r_=true; pikar_redirect=true; ${log.cookie.join().split(',')[4].split(';')[0]}; ev=1; ${tokenn[2].split(';')[0]}; ${tokenn[4].split(';')[0]}; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; insdrSV=39; ins-gaSSId=6a1bba9e-ac6e-4dc1-a625-226873ef00e5_1569993175; ${tokenn[6].split(';')[0]}`;
		const cookieKuis = await getCookieKuis(realCokk);
		const cookieKuiss = cookieKuis.cookie.join().split(',');
		const csrf2 = cookieKuis.csrf;
		const realCokkk = `scs=1; ${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; scs=1; ins-gaSSId=6a1bba9e-ac6e-4dc1-a625-226873ef00e5_1569993175; ev=1; _p1K4r_=true; pikar_redirect=true; ${log.cookie.join().split(',')[4].split(';')[0]}; ${tokenn[2].split(';')[0]}; ${tokenn[4].split(';')[0]}; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; _gat_UA-102334128-3=1; insdrSV=49; ${cookieKuiss[0].split(';')[0]}`;
		const poin = await kuis(realCokkk, csrf2);
		if (poin.data.status === true) {
			const realCokkkk = `${deviceId}; _ga=GA1.2.801524997.1569333950; _hjid=410e1028-428b-4119-bd16-4c33345cfce3; accC=true; _gid=GA1.2.956867519.1569892586; kppid_managed=M6zzHlLL; _p1K4r_=true; pikar_redirect=true; mp_41fb5b1708a7763a1be4054da0f74d65_mixpanel=%7B%22distinct_id%22%3A%20%2216d6397b49c19-0f41926c0d830b-67e1b3f-100200-16d6397b49d35f%22%2C%22%24device_id%22%3A%20%22${deviceIds}%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; ins-gaSSId=da5e1718-baf6-4476-3402-05c9dbe843bc_1570105015; ev=1; ${log.cookie.join().split(',')[4].split(';')[0]}; scs=1; ${tokenn[2].split(';')[0]}; ${tokenn[4].split(';')[0]}; ${tokenn[6].split(';')[0]}; insdrSV=71`;
			const pts = await getPoint(realCokkkk);
			console.log(email+' = Success get point 10.000. You Have '+pts+' pts');
		} else {
			console.log(email+' = Gagal get point.');
		}
	   }
    } else {
    	console.log('File Gada Isinya bg');
    }
})();